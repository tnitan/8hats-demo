package hats.dao;

/**
 * Created by acoman on 23.09.2017.
 */
public class HistoryDao {

    private Long binId;
    private String ObjectType;
    private String time;
    private String points;

    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "HistoryDao{" +
                "binId=" + binId +
                ", ObjectType='" + ObjectType + '\'' +
                ", time='" + time + '\'' +
                ", points='" + points + '\'' +
                '}';
    }
}
