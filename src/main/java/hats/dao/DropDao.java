package hats.dao;

/**
 * Created by acoman on 22.09.2017.
 */
public class DropDao {

    private Long dropId;
    private Long binId;
    private int capacity;
    private int objectType;
    private String time;
    private String points;
    private String userid;

    public Long getDropId() {
        return dropId;
    }

    public void setDropId(Long dropId) {
        this.dropId = dropId;
    }

    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "DropDao{" +
                "dropId=" + dropId +
                ", binId=" + binId +
                ", capacity=" + capacity +
                ", objectType=" + objectType +
                ", time='" + time + '\'' +
                '}';
    }
}
