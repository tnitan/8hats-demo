package hats.controller;

import hats.dao.DropDao;
import hats.model.Drop;
import hats.model.Identification;
import hats.model.Users;
import hats.repository.DropRepository;
import hats.repository.IdentificationRepository;
import hats.repository.UsersRepository;
import hats.utils.AppUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.xml.bind.DatatypeConverter;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by acoman on 22.09.2017.
 */
@RestController
public class DropController {

    @Autowired
    private DropRepository dropRepository;

    @Autowired
    private IdentificationRepository identificationRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    protected EntityManagerFactory entityManagerFactory;

    @PostMapping(value = "/drops")
    public ResponseEntity<DropDao> drop(@RequestBody DropDao dropDao) throws ParseException {

        Drop responseDrop;
        DropDao responseDropDao = new DropDao();

        Drop drop = new Drop();
        drop.setBinId(dropDao.getBinId());
        drop.setCapacity(dropDao.getCapacity());
        drop.setObjectType(dropDao.getObjectType());
        drop.setTime(java.sql.Timestamp.valueOf(dropDao.getTime()));

        try{
            responseDrop = dropRepository.save(drop);
        }catch(Exception e){
            return new ResponseEntity<DropDao>( new DropDao(), HttpStatus.BAD_REQUEST);
        }

        responseDropDao.setBinId(responseDrop.getBinId());
        responseDropDao.setCapacity(responseDrop.getCapacity());
        responseDropDao.setObjectType(responseDrop.getObjectType());
        responseDropDao.setTime(responseDrop.getTime().toString());
        responseDropDao.setDropId(responseDrop.getDropId());

        return new ResponseEntity<DropDao>(responseDropDao, HttpStatus.CREATED);
    }

    @PostMapping(value = "/payloadDrops")
    public ResponseEntity<String> payloadDrops(@RequestBody Map<String, Object> payload) throws ParseException, JSONException {

        Object json = payload.get("DevEUI_uplink");
        //String payloadHex = (String) payload.get("DevEUI_uplink").get("payload_hex");

        JSONObject jaob= new JSONObject(payload);
        String payloadHex = jaob.getJSONObject("DevEUI_uplink").getString("payload_hex");
        byte[] bytes = DatatypeConverter.parseHexBinary(payloadHex);
        String payloadString = new String(bytes);
        String[] payloadParse = payloadString.split(";");
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());

        Drop drop = new Drop();
            drop.setTime(java.sql.Timestamp.valueOf(timeStamp));
        drop.setBinId(Long.parseLong(payloadParse[0]));
        drop.setCapacity(Integer.parseInt(payloadParse[1]));
        drop.setObjectType(Integer.parseInt(payloadParse[2]));
        drop.setDropPoints("Waiting for validation");


        try{
            dropRepository.save(drop);
        }catch(Exception e){
            return new ResponseEntity<String>( new String(), HttpStatus.BAD_REQUEST);
        }


                /*
        *  correlation between drops and identification
        */
//        List<Identification> identifications = identificationRepository.findByBinId(drop.getBinId());
//        for(Identification idf : identifications){
//            if(!idf.isFlag() && idf.getBinId() == drop.getBinId()){
//
//
//                LocalDateTime l1 = drop.getTime().toLocalDateTime();
//                LocalDateTime l2 = idf.getTime().toLocalDateTime();
//
//                long duration = Duration.between(l2, l1).toMillis() / 1000;
//                Users user = usersRepository.findByUserId(idf.getUserId());
//                if(duration <= 30) {
//                    if (user != null) {
//                        AppUtils utils = new AppUtils();
//                        user.setPoints(user.getPoints() + utils.getPoints(drop.getObjectType()));
//                        idf.setFlag(true);
//                        drop.setDropPoints(String.valueOf(utils.getPoints(drop.getObjectType())));
//                        drop.setUserId(user.getUserId());
//                        dropRepository.save(drop);
//                        identificationRepository.save(idf);
//                        usersRepository.save(user);
//                    }
//                }
//            }
//        }

            Identification identification = identificationRepository.getLastIdentification();
            if(identification.getBinId() == drop.getBinId()){

                LocalDateTime l1 = drop.getTime().toLocalDateTime();
                LocalDateTime l2 = identification.getTime().toLocalDateTime();
                long duration = Duration.between(l2, l1).toMillis() / 1000;
                Users user = usersRepository.findByUserId(identification.getUserId());

                if(duration <= 30) {
                    if (user != null) {
                        AppUtils utils = new AppUtils();
                        user.setPoints(user.getPoints() + utils.getPoints(drop.getObjectType()));
                        drop.setDropPoints(String.valueOf(utils.getPoints(drop.getObjectType())));
                        drop.setUserId(user.getUserId());
                        dropRepository.save(drop);
                        usersRepository.save(user);
                    }
                }

            }





        return null;
    }



    @GetMapping(value = "/test")
    public ResponseEntity<String> test(){

        return new ResponseEntity<String>("{\"test\":\"test\"}", HttpStatus.OK);
    }
}
