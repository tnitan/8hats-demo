package hats.repository;

import hats.model.Identification;
import hats.model.Users;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by acoman on 22.09.2017.
 */
@Repository
@Qualifier(value = "identificationRepository")
public interface IdentificationRepository extends CrudRepository<Identification, String> {

    List<Identification> findByBinId(Long binId);

    @Query(value = "select i from Identification i where i.identificationId = (select max(identificationId) from Identification )")
    Identification getLastIdentification();
}
