package hats.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by acoman on 22.09.2017.
 */
@Entity
@Table( name = "identification" , schema = "hackaton")

public class Identification implements Serializable {

    private static final long serialVersionUID = -4922248200054286997L;

    @Id
    @Column(name = "identification_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long identificationId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "binId")
    private Long binId;

    @Column(name = "time")
    private Timestamp time;

    @Column(name = "flag")
    private boolean flag;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdentificationId() {
        return identificationId;
    }

    public void setIdentificationId(Long identificationId) {
        this.identificationId = identificationId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Identification{" +
                "identificationId=" + identificationId +
                ", userId=" + userId +
                ", binId=" + binId +
                ", time=" + time +
                ", flag=" + flag +
                '}';
    }
}
