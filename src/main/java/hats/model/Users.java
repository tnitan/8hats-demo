package hats.model;

import javax.persistence.*;

/**
 * Created by acoman on 22.09.2017.
 */
@Entity
@Table(name = "users", schema = "hackaton")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "points")
    private int points;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", points=" + points +
                '}';
    }
}

