package hats.dao;

import java.util.List;

/**
 * Created by acoman on 23.09.2017.
 */
public class DetailedHistoryDao {

    private List<HistoryDao> historyDaos;
    private int glassNr;
    private int platicNr;
    private int cardboardNr;

    public List<HistoryDao> getHistoryDaos() {
        return historyDaos;
    }

    public void setHistoryDaos(List<HistoryDao> historyDaos) {
        this.historyDaos = historyDaos;
    }

    public int getGlassNr() {
        return glassNr;
    }

    public void setGlassNr(int glassNr) {
        this.glassNr = glassNr;
    }

    public int getPlaticNr() {
        return platicNr;
    }

    public void setPlaticNr(int platicNr) {
        this.platicNr = platicNr;
    }

    public int getCardboardNr() {
        return cardboardNr;
    }

    public void setCardboardNr(int cardboardNr) {
        this.cardboardNr = cardboardNr;
    }

    @Override
    public String toString() {
        return "DetailedHistoryDao{" +
                "historyDaos=" + historyDaos +
                ", glassNr=" + glassNr +
                ", platicNr=" + platicNr +
                ", cardboardNr=" + cardboardNr +
                '}';
    }
}
