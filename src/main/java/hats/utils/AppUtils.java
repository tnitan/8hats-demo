package hats.utils;

/**
 * Created by acoman on 22.09.2017.
 */
public class AppUtils {

    public int getPoints(int ObjectType){

        switch (ObjectType){
            case 1:
                return 15;
            case 2:
                return 10;
            case 3:
                return 5;
        }
        return 0;
    }

    public String getItems(int ObjectType){

        switch (ObjectType) {
            case 1:
                return "Glass";
            case 2:
                return "Plastic";
            case 3:
                return "Cardboard";
        }

        return "Unidentified";
    }

}
