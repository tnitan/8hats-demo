package hats.controller;

import hats.dao.IdentificationDao;
import hats.model.Identification;
import hats.repository.IdentificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by acoman on 22.09.2017.
 */
@RestController
public class IdentificationController {

    @Autowired
    private IdentificationRepository userRepository;

    @PostMapping(value = "/identifications")
    public ResponseEntity<IdentificationDao> identification(@RequestBody IdentificationDao identificationDao) {

        Identification responseIdentification;
        IdentificationDao responseIdentficationDto = new IdentificationDao();

        Identification identification = new Identification();
        identification.setUserId(identificationDao.getUserId());
        identification.setBinId(identificationDao.getBinId());
        identification.setTime(Timestamp.valueOf(identificationDao.getTime()));
        identification.setFlag(false);


        try{
            responseIdentification = userRepository.save(identification);
        }catch(Exception e){
            return new ResponseEntity<IdentificationDao>( new IdentificationDao(), HttpStatus.BAD_REQUEST);
        }

        responseIdentficationDto.setUserId(responseIdentification.getUserId());
        responseIdentficationDto.setBinId(responseIdentification.getBinId());
        responseIdentficationDto.setTime(responseIdentification.getTime().toString());
        responseIdentficationDto.setIdentificationId(responseIdentification.getIdentificationId());

        return new ResponseEntity<IdentificationDao>(responseIdentficationDto, HttpStatus.CREATED);
    }

}
