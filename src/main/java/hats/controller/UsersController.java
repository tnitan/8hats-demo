package hats.controller;

import hats.dao.DetailedHistoryDao;
import hats.dao.HistoryDao;
import hats.model.Drop;
import hats.model.Users;
import hats.repository.DropRepository;
import hats.repository.UsersRepository;
import hats.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by acoman on 22.09.2017.
 */
@RestController
public class UsersController {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private DropRepository dropRepository;

    @RequestMapping(value="/getPoints/{id}")
    @ResponseBody
    public ResponseEntity<Users> getPoints(@PathVariable("id") Long id){

        Users user = usersRepository.findByUserId(id);

        return new ResponseEntity<Users>(user, HttpStatus.OK);
    }

    @RequestMapping(value="/getHistory/{id}")
    @ResponseBody
    public ResponseEntity<List<HistoryDao>> getHistory(@PathVariable("id") Long id){

        List<Drop> drops = dropRepository.findByUserId(id);
        AppUtils appUtils = new AppUtils();
        List<HistoryDao> historyDaos = new ArrayList<>();
        for(Drop drop : drops) {
            HistoryDao historyDao = new HistoryDao();
            historyDao.setBinId(drop.getBinId());
            String timeStamp = new SimpleDateFormat("HH:mm").format(drop.getTime());
            historyDao.setTime(timeStamp);
            historyDao.setPoints(drop.getDropPoints());
            historyDao.setObjectType(appUtils.getItems(drop.getObjectType()));
            historyDaos.add(historyDao);
        }

        return new ResponseEntity<List<HistoryDao>>(historyDaos, HttpStatus.OK);
    }

    @RequestMapping(value="/getDetailedHistory/{id}")
    @ResponseBody
    public ResponseEntity<DetailedHistoryDao> getDetailedHistory(@PathVariable("id") Long id){

        List<Drop> drops = dropRepository.findByUserId(id);
        AppUtils appUtils = new AppUtils();

        List<HistoryDao> historyDaos = new ArrayList<>();
        for(Drop drop : drops) {
            HistoryDao historyDao = new HistoryDao();
            historyDao.setBinId(drop.getBinId());
            String timeStamp = new SimpleDateFormat("HH:mm").format(drop.getTime());
            historyDao.setTime(timeStamp);
            historyDao.setPoints(drop.getDropPoints());
            historyDao.setObjectType(appUtils.getItems(drop.getObjectType()));
            historyDaos.add(historyDao);
        }

        DetailedHistoryDao detailedHistoryDao = new DetailedHistoryDao();
        detailedHistoryDao.setHistoryDaos(historyDaos);
        detailedHistoryDao.setGlassNr(dropRepository.countByUserIdAndObjectType(id,1));
        detailedHistoryDao.setPlaticNr(dropRepository.countByUserIdAndObjectType(id,2));
        detailedHistoryDao.setCardboardNr(dropRepository.countByUserIdAndObjectType(id,3));

        return new ResponseEntity<DetailedHistoryDao>(detailedHistoryDao, HttpStatus.OK);
    }

}
