package hats.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by acoman on 22.09.2017.
 */
@Entity
@Table(name = "drop", schema = "hackaton")
public class Drop implements Serializable {

    private static final long serialVersionUID = -4922248200054286997L;

    @Id
    @Column(name = "drop_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long dropId;

    @Column(name = "bin_id")
    private Long binId;

    @Column(name = "capacity")
    private int capacity;

    @Column(name = "object_type")
    private int objectType;

    @Column(name = "time")
    private Timestamp time;

    @Column(name = "points")
    private String dropPoints;

    @Column(name = "user_id")
    private Long userId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getDropId() {
        return dropId;
    }

    public void setDropId(Long dropId) {
        this.dropId = dropId;
    }

    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getDropPoints() {
        return dropPoints;
    }

    public void setDropPoints(String dropPoints) {
        this.dropPoints = dropPoints;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Drop{" +
                "dropId=" + dropId +
                ", binId=" + binId +
                ", capacity=" + capacity +
                ", objectType=" + objectType +
                ", time=" + time +
                ", dropPoints='" + dropPoints + '\'' +
                ", userId=" + userId +
                '}';
    }
}
