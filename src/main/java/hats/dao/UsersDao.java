package hats.dao;

/**
 * Created by acoman on 22.09.2017.
 */
public class UsersDao {

    private Long userId;
    private int points;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "UsersDao{" +
                "userId=" + userId +
                ", points=" + points +
                '}';
    }
}
