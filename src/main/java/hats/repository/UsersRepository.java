package hats.repository;

import hats.model.Users;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by acoman on 22.09.2017.
 */
@Repository
@Qualifier(value = "usersRepository")
public interface UsersRepository extends CrudRepository<Users, String> {

    Users findByUserId(Long userId);

}
