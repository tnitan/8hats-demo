package hats.repository;

import hats.model.Drop;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by acoman on 22.09.2017.
 */
@Repository
@Qualifier(value = "dropRepository")
public interface DropRepository extends CrudRepository<Drop, String> {

    List<Drop> findByUserId(Long id);

    int countByUserIdAndObjectType(Long userId, int objType);

}
