package hats.dao;

import java.sql.Timestamp;

/**
 * Created by acoman on 22.09.2017.
 */
public class IdentificationDao {

   private Long userId;
   private Long binId;
   private String time;
   private Long identificationId;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getIdentificationId() {
        return identificationId;
    }

    public void setIdentificationId(Long identificationId) {
        this.identificationId = identificationId;
    }

    @Override
    public String toString() {
        return "IdentificationDao{" +
                "userId=" + userId +
                ", binId=" + binId +
                ", time='" + time + '\'' +
                ", identificationId=" + identificationId +
                '}';
    }
}
